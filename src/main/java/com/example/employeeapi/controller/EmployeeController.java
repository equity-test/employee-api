package com.example.employeeapi.controller;

import com.example.employeeapi.mapper.EmployeeMapper;
import com.example.employeeapi.model.Employee;
import com.example.employeeapi.model.response.StdResponse;
import com.example.employeeapi.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api_fe")
public class EmployeeController {
    private static Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/list_employee")
    public ResponseEntity<StdResponse<List<Employee>>> getEmployeeHierarchyById(@RequestParam Integer employeeId){
        logger.info("========== START getEmployeeHierarchyById with employeeId:"+employeeId+" ==========");
        Map<String, Object> mapEmployee = employeeRepository.getEmployeeHierarchyById(employeeId);
        List<Employee> lEmployee = EmployeeMapper.mapEmployees(mapEmployee, 1);

        StdResponse<List<Employee>> response = new StdResponse<List<Employee>>(HttpStatus.OK.toString(), "", lEmployee);
        logger.info("========== END getEmployeeHierarchyById ==========");
        return new ResponseEntity<StdResponse<List<Employee>>>(response, HttpStatus.OK);
    }
}
