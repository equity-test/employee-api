package com.example.employeeapi.mapper;

import com.example.employeeapi.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmployeeMapper {
    public static List<Employee> mapEmployees(Map<String, Object> out, int resultSetPosition){
        List<Employee> employees = new ArrayList<Employee>();
        List<Map<String, Object>> results = (List<Map<String, Object>>) out.get("#result-set-" + resultSetPosition);

        results.forEach(e -> {
            Employee employee = new Employee(
                    (Integer) e.get("employee_id"),
                    (String) e.get("employee_name"),
                    (String) e.get("manager_name"),
                    (Integer) e.get("path_level"),
                    (String) e.get("employee_format"),
                    (String) e.get("path_hierarchy")
            );
            employees.add(employee);
        });

        return employees;
    }
}
