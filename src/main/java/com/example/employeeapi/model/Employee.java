package com.example.employeeapi.model;

public class Employee {
    private Integer employee_id;
    private String employee_name;
    private String manager_name;
    private Integer path_level;
    private String employee_format;
    private String path_hierarchy;

    public Employee(Integer employee_id, String employee_name, String manager_name, Integer path_level, String employee_format, String path_hierarchy) {
        this.employee_id = employee_id;
        this.employee_name = employee_name;
        this.manager_name = manager_name;
        this.path_level = path_level;
        this.employee_format = employee_format;
        this.path_hierarchy = path_hierarchy;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public Integer getPath_level() {
        return path_level;
    }

    public void setPath_level(Integer path_level) {
        this.path_level = path_level;
    }

    public String getEmployee_format() {
        return employee_format;
    }

    public void setEmployee_format(String employee_format) {
        this.employee_format = employee_format;
    }

    public String getPath_hierarchy() {
        return path_hierarchy;
    }

    public void setPath_hierarchy(String path_hierarchy) {
        this.path_hierarchy = path_hierarchy;
    }
}
