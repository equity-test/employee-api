package com.example.employeeapi.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "",
        ""
})
public class StdResponse<T> {
    @JsonProperty("status")
    public String status;
    @JsonProperty("message")
    public String message;

    @JsonProperty("data")
    public T data;

    public StdResponse(String status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
