package com.example.employeeapi.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeRepository implements IEmployeeRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Map<String, Object> getEmployeeHierarchyById(Integer employeeId){
        List<SqlParameter> parameters = Arrays.asList(new SqlParameter(Types.INTEGER));
        return jdbcTemplate.call(new CallableStatementCreator() {
            @Override
            public CallableStatement createCallableStatement(Connection con) throws SQLException {
                CallableStatement cs = con.prepareCall("{call SP_EMPLOYEE_HIERARCHY(?)}");
                cs.setInt(1, employeeId);
                return cs;
            }
        }, parameters);
    }
}