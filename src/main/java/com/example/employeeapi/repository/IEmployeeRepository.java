package com.example.employeeapi.repository;

import java.util.Map;

public interface IEmployeeRepository {
    public Map<String, Object> getEmployeeHierarchyById(Integer employeeId);
}
